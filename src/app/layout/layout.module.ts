import { LayoutComponent } from './layout.component';
import { NgModule } from '@angular/core';
import { ClientComponent } from './client/client.component';
@NgModule({
    imports: [

    ],
    exports: [],
    declarations: [LayoutComponent, ClientComponent],
    providers: []
})
export class LayoutModule { }
