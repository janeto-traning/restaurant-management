import { MenuComponent } from './layout/menu/menu.component';
import { ClientComponent } from './layout/client/client.component';
import { AdminModule } from './admin/admin.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { HomeComponent } from './home/home.component';

import { LocalStorageModule } from 'angular-2-local-storage';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginService } from './shared/login.service';
import { Http } from '@angular/http';
import {HttpModule} from '@angular/http';
import { FormsModule } from '@angular/forms';// import cái này để nó hiểu ngModel
import { HeaderComponent } from './layout/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    HomeComponent,
    ClientComponent,
    HeaderComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    LocalStorageModule.withConfig({
            prefix: 'chili-sauce',
            storageType: 'localStorage'
        }),
    RouterModule.forRoot([
      {path: '', redirectTo: 'client', pathMatch: 'full'},
      {
        path: 'client', component: LayoutComponent, children: [
          { path: '', component: ClientComponent }
        ]
      },
      { path: 'login', component: LoginComponent },
      { path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule' },
    ]),
    AdminModule,
  ],
  providers: [LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
