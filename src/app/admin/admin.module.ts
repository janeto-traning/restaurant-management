import { AdminRoutingModule } from './admin.router';
import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
@NgModule({
    imports: [
        AdminRoutingModule
    ],
    exports: [],
    declarations: [AdminComponent, DashboardComponent],
    providers: [],
})
export class AdminModule { }
