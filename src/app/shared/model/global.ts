export class Global {
    public static BASE_USER_LOGIN = 'http://localhost:54440/api/accountapi/';
    public static BASE_USER_REGISTER = 'http://localhost:54440/register';

    public static BASE_TABLE = 'http://localhost:54440/api/tableapi';
    public static BASE_TABLE_ID = 'http://localhost:54440/api/tableapi/';

    public static BASE_FOODCATEGO = 'http://localhost:54440/api/foodcatoloapi';
    public static BASE_FOODCATEGO_ID = 'http://localhost:54440/api/foodcatoloapi/';

    public static BASE_FOOD = 'http://localhost:54440/api/foodapi';
    public static BASE_FOOD_ID = 'http://localhost:54440/api/foodapi/';

    public static BASE_DRINKCATEGO = 'http://localhost:54440/api/drinkcateloapi';
    public static BASE_DRINKCATEGO_ID = 'http://localhost:54440/api/drinkcateloapi/';

    public static BASE_DRINK = 'http://localhost:54440/api/drinkapi';
    public static BASE_DRINK_ID = 'http://localhost:54440/drinkapi/';
}