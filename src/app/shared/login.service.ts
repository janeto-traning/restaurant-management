import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router, CanActivate } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';

@Injectable()
export class LoginService implements CanActivate {

  constructor(private _http: Http, private localStoraged: LocalStorageService, private router: Router) { }

  canActivate() {
    const login = this.localStoraged.get('current_user_id');
    if (!login) {
      this.router.navigate(['/login']);
      return login ? true : false;
    }
  }

  //login(url: string, user: string, password: string): Observable<any> {
  login(url: string, model: any): Observable<any> {
    const body = JSON.stringify(model);
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this._http.post(url, body, options)
      .do(data => console.log("login: " + JSON.stringify(data)))
      .map((response: Response) =>  response.json())
      // .map(data => {
      //   if (data.status === 200) {
      //     return JSON.stringify(data)
      //   }
      // })
      .catch((error: Response) => Observable.throw(error.json().error || console.log('Login Failed')));
  }
}
