import { Component, OnInit } from '@angular/core';
import { Global } from '../../shared/model/global';
import { LoginService } from '../../shared/login.service';
import { Observable } from 'rxjs/Rx';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';
import { IUser } from '../../shared/model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  usr: IUser = {
    user: '',
    pass: ''
  };
  constructor(private _login: LoginService, private localStarage: LocalStorageService, private router: Router) { }

  ngOnInit() {

  }
  login() {
    this._login.login(Global.BASE_USER_LOGIN, this.usr).subscribe(
      data => {
        console.log('login success');
        //console.log(data)
        this.localStarage.set('current_user_id', data.id);
        this.router.navigate(['/']);
      },
      error => { console.log('login failed');this.router.navigate(['/login']); }
    );
  }

}


